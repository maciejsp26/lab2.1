/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab2.Trwałość;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Maciej
 */
@Entity
@Table(name = "NOOTEBOOKS")
@NamedQueries({
    @NamedQuery(name = "Nootebooks.findAll", query = "SELECT n FROM Nootebooks n"),
    @NamedQuery(name = "Nootebooks.findByNotebookId", query = "SELECT n FROM Nootebooks n WHERE n.notebookId = :notebookId"),
    @NamedQuery(name = "Nootebooks.findByName", query = "SELECT n FROM Nootebooks n WHERE n.name = :name"),
    @NamedQuery(name = "Nootebooks.findByPricePln", query = "SELECT n FROM Nootebooks n WHERE n.pricePln = :pricePln"),
    @NamedQuery(name = "Nootebooks.findByPages", query = "SELECT n FROM Nootebooks n WHERE n.pages = :pages"),
    @NamedQuery(name = "Nootebooks.findByFormat", query = "SELECT n FROM Nootebooks n WHERE n.format = :format"),
    @NamedQuery(name = "Nootebooks.findByManufacturer", query = "SELECT n FROM Nootebooks n WHERE n.manufacturer = :manufacturer"),
    @NamedQuery(name = "Nootebooks.findByHasNumberedPages", query = "SELECT n FROM Nootebooks n WHERE n.hasNumberedPages = :hasNumberedPages")})
public class Nootebooks implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NOTEBOOK_ID")
    private Integer notebookId;
    @Column(name = "NAME")
    private String name;
    @Column(name = "PRICE_PLN")
    private Integer pricePln;
    @Column(name = "PAGES")
    private Integer pages;
    @Column(name = "FORMAT")
    private String format;
    @Column(name = "MANUFACTURER")
    private String manufacturer;
    @Column(name = "HAS_NUMBERED_PAGES")
    private Boolean hasNumberedPages;

    public Nootebooks(Integer notebookId, String name, Integer pricePln, Integer pages, String format, String manufacturer, Boolean hasNumberedPages) {
        this.notebookId = notebookId;
        this.name = name;
        this.pricePln = pricePln;
        this.pages = pages;
        this.format = format;
        this.manufacturer = manufacturer;
        this.hasNumberedPages = hasNumberedPages;
    }

    public Nootebooks(Integer notebookId) {
        this.notebookId = notebookId;
    }

    public Integer getNotebookId() {
        return notebookId;
    }

    public void setNotebookId(Integer notebookId) {
        this.notebookId = notebookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPricePln() {
        return pricePln;
    }

    public void setPricePln(Integer pricePln) {
        this.pricePln = pricePln;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Boolean getHasNumberedPages() {
        return hasNumberedPages;
    }

    public void setHasNumberedPages(Boolean hasNumberedPages) {
        this.hasNumberedPages = hasNumberedPages;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notebookId != null ? notebookId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nootebooks)) {
            return false;
        }
        Nootebooks other = (Nootebooks) object;
        if ((this.notebookId == null && other.notebookId != null) || (this.notebookId != null && !this.notebookId.equals(other.notebookId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        //return "Lab2.Trwa\u0142o\u015b\u0107.Nootebooks[ notebookId=" + notebookId + " ]";
        return "Lab2.Trwałość.Nootebooks[ notebookId=" + notebookId + " ]";
    }
    
}
