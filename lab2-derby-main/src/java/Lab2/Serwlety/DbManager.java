/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Lab2.Serwlety;

//import jakarta.persistence.EntityManager;
//import jakarta.persistence.EntityManagerFactory;
//import jakarta.persistence.Persistence;
//import jakarta.persistence.TypedQuery;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
//import java.util.List;

public final class DbManager {
 public static final String DRIVER = "org.apache.derby.jdbc.ClientDriver";
 public static final String JDBC_URL = "jdbc:derby://localhost:1527/B:\\Studia\\STUDIA lll ROK\\Semestr 6\\Technologie JavaEE\\2.1 try\\lab2-master\\lab1-derby-main\\db\\notebooks";
 public static final String QUERY = "select * from NOOTEBOOKS";
 private static java.sql.Connection conn;
 
 private DbManager() {  
    //EntityManagerFactory emf = Persistence.createEntityManagerFactory("Nazwa PU");
    //EntityManager em = emf.createEntityManager();
    //TypedQuery<Lab2.Trwałość.Nootebooks> query = em.createNamedQuery("Nootebooks.findAll",Lab2.Trwałość.Nootebooks.class);
 }
        
 public static boolean Connect() throws ClassNotFoundException, SQLException {
     
 conn = DriverManager.getConnection(JDBC_URL);
    if (conn == null) 
    {
        return false;
    } 
    else 
    {
           return true;
    }
 }
 public static boolean Disconnect() throws SQLException {
    if (conn == null) 
    {
        return false;
    } 
    else 
    {
        conn.close();
        return true;
    }
 }

 
 public static String getData() throws SQLException {
    Statement stat = conn.createStatement();
    ResultSet rs = stat.executeQuery(QUERY);
    ResultSetMetaData rsmd = rs.getMetaData();
    String wiersz = new String();
    
    int colCount = rsmd.getColumnCount();
    wiersz = wiersz.concat("<table><tr>");
    
    for (int i = 1; i <= colCount; i++) 
    {
        wiersz = wiersz.concat(" <td><b> " + rsmd.getColumnName(i) + "</b></td> ");
    }
    wiersz = wiersz.concat("</tr>");
 
    while (rs.next()) 
    {
        wiersz = wiersz.concat("</tr>");
        for (int i = 1; i <= colCount; i++) {
        wiersz = wiersz.concat(" <td> " + rs.getString(i) + " </td> ");
        }
        wiersz = wiersz.concat("</tr>");
    }
    wiersz = wiersz.concat("</table>");
    
    if (stat != null) { stat.close(); } return wiersz; } 
}

   