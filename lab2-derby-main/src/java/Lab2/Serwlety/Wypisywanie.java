/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Lab2.Serwlety;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maciej
 */

@WebServlet(name = "Wypisywanie", urlPatterns = {"/Wypisywanie"})
public class Wypisywanie extends HttpServlet {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("lab2-derby-mainPU");
    EntityManager em = emf.createEntityManager();
    //TypedQuery<Lab2.Trwałość.Nootebooks> query = em.createNamedQuery("Nootebooks.findAll",Lab2.Trwałość.Nootebooks.class);
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        String inst=request.getParameter("instytucja");
        List<Lab2.Trwałość.Nootebooks> result;
        result = em.createNamedQuery("Nootebooks.findAll",Lab2.Trwałość.Nootebooks.class).getResultList();
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head><meta><link rel='stylesheet' href='Style/css/components.css'>");
            out.println("<link rel='stylesheet' href='Style/css/icons.css'>");
            out.println("<link rel='stylesheet' href='Style/css/responsee.css'>");
            out.println("<body>" + "Lista pracowników instytucji: " + inst + "<br />");
            out.println(getListaHTML(result));
            out.println("</body></html>");
            out.println("<a class=\'button rounded-full-btn reload-btn s-2 margin-bottom\' href=");
            out.println(request.getHeader("referer"));
            out.println("><i class='icon-sli-arrow-left'>Powrót</i></a>");
        }
    }

    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean polaczenie = false;
    private String data;
    
    private String getDataFromDb() {
        try{
            polaczenie = Lab2.Serwlety.DbManager.Connect();
            if (polaczenie) {
                data = Lab2.Serwlety.DbManager.getData();
                polaczenie = Lab2.Serwlety.DbManager.Disconnect();
            }
        }
        catch (ClassNotFoundException | SQLException ex ) {
            Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }
    
        public static String getListaHTML(List<Lab2.Trwałość.Nootebooks> lista) {
        String wiersz;        
        wiersz = ("<table><tr>");        
        wiersz = wiersz.concat("<td><b>ID</b></td>"                
            + "<td><b>IMIE</b></td>"                
            + "<td><b>NAZWISKO</b></td>");       
        wiersz = wiersz.concat("</tr>");
        for (Lab2.Trwałość.Nootebooks ldz : lista) 
        {            
            wiersz = wiersz.concat("<tr>");           
            wiersz = wiersz.concat("<td>" + ldz.getNotebookId() + "</td>");            
            wiersz = wiersz.concat("<td>" + ldz.getName() + "</td>");            
            wiersz = wiersz.concat("<td>" + ldz.getPricePln() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getPages() + "</td>");   
            wiersz = wiersz.concat("<td>" + ldz.getFormat() + "</td>"); 
            wiersz = wiersz.concat("<td>" + ldz.getManufacturer() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getHasNumberedPages() + "</td>"); 
            wiersz = wiersz.concat("</tr>");
        }        
        wiersz = wiersz.concat("</table>");
        return wiersz;    
    } 
}
